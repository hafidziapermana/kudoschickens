// Problems
// Given 1 Male
// Given 2 Female, @Female produce 3 egg each day
// Count egg production @Week basis

// Kudo egg solution
// Run : node egg.js

const egg = {
  // Lets Go 
  week: 7,
  produce: function(male, female, weeks) {
    
    weeks = this.week * weeks
    let sum = (female * 3) * weeks
    
    if (typeof male !== 'number') {
      console.log('Error: female will always produce egg although the male is existing or not, but u have to define male a number type !!')
    } 

        else if (typeof female !== 'number') {
          console.log('Error: female should be a number')
        } 

          else if (typeof weeks !== 'number') {
            console.log('Error: week should be a number')
          } 
    
            else {
              return console.log(sum + ' eggs in ' + weeks + ' day' + '   => produced by ' + female + ' female and ' + male + ' male')
            }
  } 
}

egg.produce(0, 1, 1)
egg.produce(1, 0, 1)
egg.produce(1, 1, 2)
egg.produce('wow', 1, 9)